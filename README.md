![I.Systems](assets/IS_IMG_Logo.png)

> **Termo de confidencialidade**
>
> As imagens, gráficos e informações contidas neste documento constituem propriedade intelectual da
> I.Systems, sendo resultados diretos de pesquisas e esforços de desenvolvimento privado, desta forma,
> elas não podem ser transmitidas ou reproduzidas inteiramente ou parcialmente sem autorização prévia
> por escrito.

# Case técnico - DevOps

**Estamos mais do que felizes em ver que você está interessado em nosso desafio!**

Esta etapa é feita para conhecermos um pouco mais de cada candidato ao cargo de DevOps na I.Systems. Não se trata de uma prova objetiva, capaz de gerar uma nota ou uma taxa de sucesso, mas sim um estudo de caso com a finalidade de explorar os seus conhecimentos, experiências e forma de trabalhar. Sinta-se à vontade para desenvolver sua solução para o problema proposto. Você pode nos enviar suas dúvidas: não há limite de perguntas e nos comprometemos a enviar uma resposta em até 24h, utilize essa liberdade para validar suas ideias e não ficar “travado” durante a resolução do case.

👉 [Veja todas as nossas vagas em aberto](isystems.gupy.io)

## #01 Desafio prático

Dentro do diretório _**app**_, há um aplicativo muito simples escrito em Python para expor a API de uma _todo-list_ através dos endpoins `/todos` e `/todos/<todo_id>`. Seu objetivo é implantar este aplicativo em um cluster Kuberntes utilizando Terraform. Estes são os pontos de atenção:

* O aplicativo deve ser executado em contêineres docker.
* Você deve utilizar um servidor WSGI para servir o aplicativo. Não esperamos um servidor específico você define qual prefere utilizar.
* A API do aplicativo _todo-list_ deve ficar exposta para fora do cluster e ser acessível no host local.
* Esperamos que o código IaC seja escrito usando Terraform. Dentro do diretório _**infrastructure**_ há um provider configurado para lhe dar um ponto de partida. 
* Se você tem conhecimento de desenvolvimento em Python, sinta-se à vontade para implementar ou sugerir melhorias simples no aplicativo para torná-lo pronto para produção.

Não esperamos uma solução pronta para produção, mas esperamos que você demonstre que seria capaz de implantar um sistema distribuído de nível de produção tendo acesso à ferramentas e tempo suficiente. Ao final crie um arquivo `usage.md` documentando sua solução e descrevendo as etapas necessárias para a executarmos em nosso ambiente. 

### Ambiente de desenvolvimento

Na pasta **.devcontainer** estão as configurações necessárias para disponibilizar de forma automática um ambiente de desenvolvimento virtualizado que pode ser utilizado com o VSCode. Ele contém as ferramentas que julgamos mais importantes e necessárias para desenvovler uma solução para o desafio proposto: **Python 3.9**, **kubectl** e **Terraform**. Assim você não precisa se preocupar com a configuração do ambiente e pode partir direto para elaborar sua proposta de solução!

Para utilizar este ambiente você precisará instalar o [VSCode](https://code.visualstudio.com/download) e o [Docker Desktop](https://www.docker.com/products/docker-desktop) e habilitar um cluster kubernetes local através do Docker Desktop:

![kubernetes](assets/docker_desktop_kubernetes.gif)

Depois é só abrir a IDE na pasta em que clonou este repositório e clicar na opção **Reopen in Container** que aparece na parte inferior da tela:

![dev_container_prompt](assets/reopen-in-container.gif)

Bom desenvolvimento!

## #02 Desafio teórico

Considere a solução e as ferramentas utilizadas no desafio anterior. Agora o seu objetivo é elaborar uma proposta de infraestrutura contemplando os componentes necessários para implantar um cluster Kubernetes gerenciado e um pipeline de CD para disponibilizar nosso aplicativo _todo-list_ utilizando a cloud da AWS. Inclua na sua proposta os componentes necessários para gerenciar o versionamento e a distribuição do código da aplicação e da infraestrutura como código.

Ao final crie um arquivo `cloud.md` documentando e justificando a sua proposta. **Um diagrama da arquitetura, indicando os principais componentes e conexões nos ajuda bastante a interpretar a solução.**

## Instruções finais

1. **O case deve ser resolvido apenas por você:** o mais importante para nós não é necessariamente uma resposta correta, mas sim qual o raciocínio empregado. Por isso, nenhum tipo de plágio ou consulta a pessoas externas ao Processo Seletivo é permitido.
1. **Sinta-se à vontade para entrar em contato se tiver alguma dúvida.** Além disso, esperamos que esse problema demore no máximo algumas horas para ser resolvido, mas entre em contato com recrutamento@i.systems.com.br se precisar de mais tempo para fornecer uma boa solução! É muito melhor do que entregar algo que não funciona :-)
